<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Electricity Calculation</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="js/bootstrap.min.js">
	<style type="text/css">
		.container
		{
			font-weight: 500;
		}
		.display
		{
			padding: 20px;
			border: 1px solid #89CFF0;
			border-radius: 4px;
		}
		.displayContent
		{
			color: #00008B;
			font-weight: 700;
		}

	</style>
</head>
<body>
	<div class="container mt-5">
		<div class="col-lg-6 md-12 sm-12">
			<form action="" method="post">
				<h3>Calculate</h3>
				<br>
				<?php
				if (isset($_POST['calculate'])) 
			 	{
			 	?>
			 		<label for="Voltage" class="form-label">Voltage</label>
					<input type="text" class="form-control" name="voltage" value="<?php echo $_POST['voltage']?>">
					<label for="volTageSymbol" class="form-label">Voltage(V)</label>
					<br>
					<label for="Current" class="form-label mt-3">Current</label>
					<input type="text" class="form-control" name="current" value="<?php echo $_POST['current']?>">
					<label for="volTageSymbol" class="form-label">Ampere(A)</label>
					<br>
					<label for="CurrentRate" class="form-label mt-3">CURRENT RATE</label>
					<input type="text" class="form-control" name="currentRate" value="<?php echo $_POST['currentRate']?>">
					<label for="currentRateSymbol" class="form-label">sen/kWh</label>
				<?php
			 	}
			 	else
			 	{
				?>
					<label for="Voltage" class="form-label">Voltage</label>
					<input type="text" class="form-control" name="voltage">
					<label for="volTageSymbol" class="form-label">Voltage(V)</label>
					<br>
					<label for="Current" class="form-label mt-3">Current</label>
					<input type="text" class="form-control" name="current">
					<label for="volTageSymbol" class="form-label">Ampere(A)</label>
					<br>
					<label for="CurrentRate" class="form-label mt-3">CURRENT RATE</label>
					<input type="text" class="form-control" name="currentRate">
					<label for="currentRateSymbol" class="form-label">sen/kWh</label>
				<?php
				}
				?>
				<br>
				<div class="text-center">
					<input type="submit" name="calculate" value="calculate" class="btn btn-outline-primary mt-2 mb-5">
				</div>
			</form>

		<?php
			if (isset($_POST['calculate'])) 
			{
			 	$voltage = floatval($_POST['voltage']);
			 	$current = floatval($_POST['current']);
			 	$currentRate = floatval($_POST['currentRate']);

			 	function calculate($voltage, $current, $currentRate)
			 	{
			 		$powerDisplay = ($voltage * $current) /1000;
			 		$currentRate = $currentRate/100;

			 		return array($powerDisplay, $currentRate);
			 	}

			 	list($powerDisplay, $currentRate) = calculate($voltage, $current, $currentRate);
			 	?>
			 	
			<div class="display">
				<h6 class="displayContent">POWER : <?php echo $powerDisplay;?>kw</h6>
				<h6 class="displayContent mt-3">RATE : <?php echo $currentRate;?>RM</h6>	
			</div>
			
			<table class="table mt-5">
			  	<thead>
				    <tr>
				      <th scope="">#</th>
				      <th scope="col">Hour</th>
				      <th scope="col">Energy (kWh)</th>
				      <th scope="col">TOTAL (RM)</th>
				    </tr>
			  	</thead>
			  	<tbody>
			 	<?php
			 	for($i = 1; $i <=24; $i++)
			 	{

			 		$energy = $powerDisplay * $i;
			 		$total = $energy * $currentRate;
			 		$total = round($total, 2);
			 		?>
			 		<tr>
			 			<th scope="row"><?php echo $i; ?></th>
			 			<td><?php echo $i; ?></td>
			 			<td><?php echo $energy; ?></td>
			 			<td><?php echo $total; ?></td>
			 		</tr>
			 	<?php
			 	}
			 	?>
			 	</tbody>
			</table>
			 <?php 
			}
			?>
		</div>
	</div>

</body>
</html>